# Python Project with linter

Python linting Docker image to be used in linting gitlab CI

## Usage

Runinng this project CI on gitlab will build a container (in `build`stage of the pipeline).
In `test`stage of the CI, we are using this container to run the different linter tools (Black, Flake8, Pylint).
The goal of this is to avoid adding dev tools in the package directly.

## Credits

This repo is freely inspired from [Pipeline Components](https://pipeline-components.dev/), in particular this [one](https://gitlab.com/pipeline-components/pylint.git), and this [other repo](https://gitlab.com/mafda/gitlab-ci-pipeline/)

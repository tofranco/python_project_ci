FROM python:3.10-alpine3.18 as build
# TODO get python version from Gammalearn env
ENV PYTHONUSERBASE /app
ENV PATH "$PATH:/app/bin/"

WORKDIR /app/
COPY requirements.txt /app/ 

RUN \
    apk add --no-cache build-base && \
# hadolint ignore=DL3013
    pip3 install --user --no-cache-dir --prefer-binary -r requirements.txt

FROM python:3.10-alpine3.18
ENV PATH "$PATH:/app/bin/"
ENV PYTHONUSERBASE /app
COPY --from=build /app /app

# ==============================================================================
# Container meta information
# ------------------------------------------------------------------------------
    ARG BUILD_DATE
    ARG BUILD_REF
    
    # Labels
    LABEL \
        org.label-schema.description="Linter in a container for gitlab-ci" \
        org.label-schema.build-date=${BUILD_DATE} \
        org.label-schema.vcs-ref=${BUILD_REF}
    